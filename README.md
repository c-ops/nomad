# Nomad notes

## Simple and cheap Nomad

Very simple but working nomad deployment (usable for management of single node with legacy applications). Because all service are working during nomad binary downtime, this solution may work, but in single node case you can use most of nomad advantages.

[Simple Nomad documentation](./tiny/README.md)


## Production Nomad cluster

Very simple but working nomad deployment (usable for management of single node with legacy applications)

[Simple Nomad documentation](./cluster/README.md)


