server {
    enabled = true
    bootstrap_expect = 1
    encrypt = "SECRET_KEY"
}