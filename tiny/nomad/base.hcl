# /etc/nomad/nomad.hcl
name = "node1"
datacenter = "dc1"
bind_addr = "0.0.0.0"
enable_debug = false

data_dir = "/home/nomad"

acl {
    enabled = true
}