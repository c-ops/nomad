client {
    enabled = true

    # reserve resources for OS under nomad
    reserved {
        cpu = 500
        memory = 512
        disk = 8000
    }

    # I use this options
    options = {
        "driver.blacklist" = "java"
        "driver.raw_exec.enable" = "1"
        "docker.volumes.enabled" = "1"
        "docker.privileged.enabled" = "True"
    }
}