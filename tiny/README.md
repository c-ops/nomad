# Tiny Nomad deployment

I know, this is not the recomended topology and configuration. But sometime you have only one or two single servers, small budget and you don't need all the cool features. 

Nomad is great orchestrator for many kinds of workloads and if you looking for something that provide you an API and nice UI for managing mix of services on the server (from simple binaries to docker containers). This is exactly what are you looking for!!!


## What is included?

This is primary my notes from nomad research. But if you follows these configs and readme, you get working single node nomad cluster secured by ACLs, with traefik ingress controller and prometheus metrics. I use this as a core of my personal nomad cluster where the rest of nodes run only when I need it


## Requirements

 * 1x VM with 1VCPU and 2G of RAM (you know,2VCPUs and 4G is better)
 * Linux (I prefer alpine or fedora but we will use the Debian 10 there because it is more common)
 * basics of bash, linux, systemd and networking
 

I run it on e2-medium in GCP


## Lets start (day 1)

### Prepare Linux

Install required and usefull tools

```bash
apt update
apt upgrade
apt install vim git tree unzip curl wget htop net-tools iproute2 rsync
```

A few of the packages above have been already installed probably.


### Install docker

you can skip this step in case you are sure that you can't use docker. But I uses docker for ingress controler so I recomend to do this step (it is easy to run traefik without docker)

```bash
apt install docker.io

# verify the installation
systemctl status docker

docker run -it alpine sh

```

### Install Nomad 
 
 1. **Create user nomad**. I don't run nomad under the root as well as other services)
```bash
useradd -m nomad -G docker --shell /bin/false --system
```

 2. **Download nomad** binary and unpack it to /usr/local/bin/nomad
```bash
 wget https://releases.hashicorp.com/nomad/1.0.0/nomad_1.0.0_linux_amd64.zip -O nomad.zip
 unzip nomad.zip -d /usr/local/bin/
 chmod +x /usr/local/bin/nomad
```

 3. **Prepare nomad config**. I like to decompose configuration to small files, because. But you can put all files in /etc/nomad together.

 ```bash
 mkdir /etc/nomad
 cp ./nomad/base.hcl /etc/nomad/base.hcl
 cp ./nomad/client.hcl /ect/nomad/client.hcl
 cp ./nomad/server.hcl /etc/nomad/server.hcl
 chown nomad:nomad -R /etc/nomad

 ```
 4. **Test nomad** configuration
 ```bash
 nomad agent -config=/etc/nomad 

 # stop it by <ctrl> + c 
 ```

 5. **Create nomad service**
 ```bash
 cp nomad.service /etc/systemd/system/nomad.service
 systemctl daemon-reload
 systemctl enable nomad
 systemctl start nomad

 # Validate again that nomad is running
 systemctl status nomad
 curl -kL http://localhost:4646
 ```


### Basic Nomad ACL

The Nomad is now running and ready to run any job, but you need to secure it a bit more, because everyone who can access the nomad server on port 4646 can do with nomad almost everything!!! 

So lets do at least a few steps as follows:

1. **Bootstrap ACL**
 ```bash
 nomad acl bootstrap | tee > /root/bootstrap
 ```
 Now is your nomad secured by token and only persons have this token can manege the nomad. So keep the bootsrap output at well secured place.
 

2. **Use the token**
You can access nomad localy or remote, if you can use it by cli client only localy you don't need export NOMAD_ADDR.

```
 export NOMAD_ADDR="http://server_adress:4646"
 export NOMAD_TOKEN="Secret ID"

 nomad agent-info
```


So now you have a nice working tiny nomad which is cute minimalist solution but it needs a bit more care to push it to production ready runtime.


## Keep going (day 2)

In this part we start Consul with ACLs and connect it together with Nomad.

### Install Cosnul
It is almost same proces so just in short:
 

```bash
# prepare user
useradd -m consul --shell /bin/false --system

# install binary
wget https://releases.hashicorp.com/consul/1.9.0-beta1/consul_1.9.0-beta1_linux_amd64.zip -C consul.zip
unzip consul.zip -d /usr/local/bin/
chmod +x /usr/local/bin/consul 

# Prepare configuration
mkdir /etc/consul
cp ./consul/base.hcl /etc/consul/base.hcl
chown consul:consul -R /etc/consul

# test configuration
consul agent -config=/etc/consul
# stop it by <ctrl> + c 

# Create nomad service
cp consul.service /etc/systemd/system/consul.service
systemctl daemon-reload
systemctl enable consul
systemctl start consul

# Validate again that consul is running
systemctl status consul
curl -kL http://localhost:8500

```

